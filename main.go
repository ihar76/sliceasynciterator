package sliceAsyncIterator

import "reflect"

type (
	SliceChan struct {
		userHandler Handler
	}
	Handler  func(...interface{}) interface{}
	Response chan interface{}
)

func GetIterator(handl Handler, params interface{}) Response {

	sc := new(SliceChan)
	sc.userHandler = handl
	sc.chanList(params)
	return sc.runChan(sc.chanList(params))
}

// Создаем канал, куда в фоне по одному закидываем наши объекты
func (sc *SliceChan) chanList(zplCh interface{}) <-chan interface{} {
	out := make(Response)
	go func() {
		for _, item := range InterfaceSlice(zplCh) {
			item := item
			out <- item
		}
		close(out)
	}()
	return out
}

// создаем канал, который отдает результаты пока не закроется канал приема объектов ( chanList )
func (sc *SliceChan) runChan(in <-chan interface{}) chan interface{} {
	out := make(chan interface{})
	go func() {
		chReturn := make(chan bool)
		i := 0
		for n := range in {
			i++
			go func(n interface{}) {
				out <- sc.userHandler(n)
				chReturn <- true
			}(n)
		}
		for j := 0; j < i; j++ {
			<-chReturn
		}
		close(out)
	}()
	return out
}

func InterfaceSlice(slice interface{}) []interface{} {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		panic("InterfaceSlice() given a non-slice type")
	}

	ret := make([]interface{}, s.Len())

	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}

	return ret
}
